// jQuery for Drupal 7 [BEGIN]
(function ($) {
	Drupal.behaviors.takealookFieldsetSummeries = {
		attach: function (context) {
			// Provide the vertical tab summaries.
			$('fieldset.invisible-content', context).drupalSetSummary(function (context) {
				//var vals = [];
				enabled = $('input:radio[name="invisible_content_type"]:checked').val();
				if(enabled==0){
					return Drupal.t('Visible');
				}else{
					return Drupal.t('Invisible');
				}
			});
		}
	};
// jQuery for Drupal 7 [END]
})(jQuery);